import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import Amplify, { API, graphqlOperation } from 'aws-amplify';
import awsconfig from './aws-exports';
import { AmplifySignOut, withAuthenticator } from '@aws-amplify/ui-react';
import { listDigitalEntrys } from './graphql/queries'
import Paper from '@material-ui/core/Paper'; 
import { IconButton } from '@material-ui/core';
import FingerprintIcon from '@material-ui/icons/Fingerprint';

Amplify.configure(awsconfig);

function App() {
  const [ digitalEntries, setDigitalEntries] = useState([]);
  
  useEffect(() => {
      fetchDigitalEntries();
  }, [])

  const fetchDigitalEntries = async () => {
    try {
          const digitalEntryData = await API.graphql(graphqlOperation(listDigitalEntrys));
          const digitalEntryList = digitalEntryData.data.listDigitalEntrys.items;
          console.log('Digital Entry List', digitalEntryList);
          setDigitalEntries(digitalEntryList);
    } catch (error) {
      console.log('error on fetching digital entires', error);

    }
  };

  return (
    <div className="App">
      <header className="App-header">
        <AmplifySignOut/>
          <h2> Initial Setup for the app funtions</h2>
      </header>
      <div className="digitalEntryList">
          { digitalEntries.map(digitalEntry => {
              return <Paper variant="outlined" elevation={2} > 
                <div className="digitalEntryCard">
                <IconButton aria-label="view">
                  <FingerprintIcon/>
                </IconButton>ping 
                </div>
              </Paper>;
            })}
      </div>
    </div>
  );
}

export default withAuthenticator(App);
