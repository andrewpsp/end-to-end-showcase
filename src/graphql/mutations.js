/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createDigitalEntry = /* GraphQL */ `
  mutation CreateDigitalEntry(
    $input: CreateDigitalEntryInput!
    $condition: ModelDigitalEntryConditionInput
  ) {
    createDigitalEntry(input: $input, condition: $condition) {
      id
      name
      region
      zip
      state
      createdAt
      updatedAt
    }
  }
`;
export const updateDigitalEntry = /* GraphQL */ `
  mutation UpdateDigitalEntry(
    $input: UpdateDigitalEntryInput!
    $condition: ModelDigitalEntryConditionInput
  ) {
    updateDigitalEntry(input: $input, condition: $condition) {
      id
      name
      region
      zip
      state
      createdAt
      updatedAt
    }
  }
`;
export const deleteDigitalEntry = /* GraphQL */ `
  mutation DeleteDigitalEntry(
    $input: DeleteDigitalEntryInput!
    $condition: ModelDigitalEntryConditionInput
  ) {
    deleteDigitalEntry(input: $input, condition: $condition) {
      id
      name
      region
      zip
      state
      createdAt
      updatedAt
    }
  }
`;
