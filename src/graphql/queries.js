/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getDigitalEntry = /* GraphQL */ `
  query GetDigitalEntry($id: ID!) {
    getDigitalEntry(id: $id) {
      id
      name
      region
      zip
      state
      createdAt
      updatedAt
    }
  }
`;
export const listDigitalEntrys = /* GraphQL */ `
  query ListDigitalEntrys(
    $filter: ModelDigitalEntryFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listDigitalEntrys(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        region
        zip
        state
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
