/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateDigitalEntry = /* GraphQL */ `
  subscription OnCreateDigitalEntry {
    onCreateDigitalEntry {
      id
      name
      region
      zip
      state
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateDigitalEntry = /* GraphQL */ `
  subscription OnUpdateDigitalEntry {
    onUpdateDigitalEntry {
      id
      name
      region
      zip
      state
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteDigitalEntry = /* GraphQL */ `
  subscription OnDeleteDigitalEntry {
    onDeleteDigitalEntry {
      id
      name
      region
      zip
      state
      createdAt
      updatedAt
    }
  }
`;
